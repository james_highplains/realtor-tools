package com.jsoft.realtools;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jsoft.realtools.RealtorToolsContract;

/**
 * Created by James on 7/21/2016.
 */
public class RealToolsDbHelper extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION = 14;
    public static final String DATABASE_NAME = "realtools.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String REAL_TYPE = " REAL";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String BLOB_TYPE = " BLOB";

    private static final String SQL_CREATE_DEAL_TABLE =
            "CREATE TABLE " + RealtorToolsContract.DealTable.TABLE_NAME + " ("+
            RealtorToolsContract.DealTable._ID + " INTEGER PRIMARY KEY," +
            RealtorToolsContract.DealTable.COLUMN_NAME_DEAL_NAME + TEXT_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_ACTIVE + TEXT_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SALE_PRICE + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_ADD_CASH + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_CLOSING_COST + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_BUYER_COMM_PERC + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_BUYER_COMM + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SELLER_COMM_PERC + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SELLER_COMM + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_TOTAL_OWED + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_TOTAL_DOWN + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_NET_PROCEEDS + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SALE_PRICE + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_MORT_AMOUNT + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_LTV + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_BUYER_COMM_PERC + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_BUYER_COMM + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SELLER_COMM_PERC + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SELLER_COMM + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_CLOSING_COSTS + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_PRIMARY_CONTACT_ID + TEXT_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_ALT_CONTACT_ID + TEXT_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_EST_INS + REAL_TYPE + COMMA_SEP+
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_EST_TAXES + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_APR + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_PI + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_PITI + REAL_TYPE + COMMA_SEP +
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_TERM + INTEGER_TYPE +
            " )";
    private static final String SQL_CREATE_SETTINGS_TABLE =
            "CREATE TABLE " + RealtorToolsContract.SettingsTable.TABLE_NAME + " ("+
            RealtorToolsContract.SettingsTable._ID+ " INTEGER PRIMARY KEY,"+
            RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE + TEXT_TYPE + COMMA_SEP +
            RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE + TEXT_TYPE + COMMA_SEP +
            RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_BLOB + BLOB_TYPE +
            " )";

    private static final String SQL_DELETE_DEAL_TABLE =
                "DROP TABLE IF EXISTS " + RealtorToolsContract.DealTable.TABLE_NAME;
    private static final String SQL_DELETE_SETTINGS_TABLE =
                "DROP TABLE IF EXISTS " + RealtorToolsContract.SettingsTable.TABLE_NAME;


    public RealToolsDbHelper(Context context)
    {
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_DEAL_TABLE);
        db.execSQL(SQL_CREATE_SETTINGS_TABLE);

        populateData(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_DEAL_TABLE);
        db.execSQL(SQL_DELETE_SETTINGS_TABLE);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onConfigure(SQLiteDatabase db){
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    private void populateData(SQLiteDatabase db)
    {
        //SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        String selection = RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE + " = ?";
        String[] selectionArgs = {"AGENT_NAME"};


        //Set the agent name
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_NAME");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,"");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);


        //set the agent phone
        selectionArgs[0] = "AGENT_PHONE";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_PHONE");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,"");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);

        //set the agent photo
        selectionArgs[0] = "AGENT_PHOTO";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_PHOTO");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,"");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_BLOB,"");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);


        //set the default buy perc
        selectionArgs[0] = "DEFAULT_BUY_COMM_PERC";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"DEFAULT_BUY_COMM_PERC");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,".028");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);


        //set the default sell perc
        selectionArgs[0] = "DEFAULT_SELL_COMM_PERC";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"DEFAULT_SELL_COMM_PERC");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,".032");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);


        //set the agent email
        selectionArgs[0] = "AGENT_EMAIL";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_EMAIL");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,"");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);

        //set the agent text line 1
        selectionArgs[0] = "AGENT_TEXT_LINE1";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_TEXT_LINE1");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,"");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);
        //set the agent text line 2
        selectionArgs[0] = "AGENT_TEXT_LINE2";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_TEXT_LINE2");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,"");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);
        //set the agent text line 3
        selectionArgs[0] = "AGENT_TEXT_LINE3";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_TEXT_LINE3");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,".");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);
        //set the default APR
        selectionArgs[0] = "DEFAULT_APR";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"DEFAULT_APR");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,".05");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);
        //set the default closing cost percent
        selectionArgs[0] = "DEFAULT_CLOSING_COST_PERC";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"DEFAULT_CLOSING_COST_PERC");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,".01");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);
        //set the default closing cost amount
        selectionArgs[0] = "DEFAULT_CLOSING_COST_AMOUNT";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"DEFAULT_CLOSING_COST_AMOUNT");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,"0.0");
        db.insert(RealtorToolsContract.SettingsTable.TABLE_NAME,null,values);

    }
}
