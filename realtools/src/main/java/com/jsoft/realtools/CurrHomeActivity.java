package com.jsoft.realtools;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Build;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class CurrHomeActivity extends Activity implements CommissionPopupFragment.OnCommissionPercentSet {

    private Deal d;
    private long curr_deal_id;

    public CurrHomeActivity()
    {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Bundle extras = getIntent().getExtras();
        curr_deal_id = extras.getLong("CURR_DEAL_ID");

        if (curr_deal_id == 0) {
            d = new Deal();
            DealToolFileHelper.applyDealDefaults(this,d);
        }
        else
            d = DealToolFileHelper.getCurrentDeal(curr_deal_id,this);

        setData();

        final Button commissionButton = (Button) findViewById(R.id.changeComm);
        final Button okButton = (Button) findViewById(R.id.CURR_HOME_OK_BUTTON);

        commissionButton.setOnClickListener(new View.OnClickListener(){
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            public void onClick(View v)
            {
                FragmentManager manager = getFragmentManager();
                Fragment frag = manager.findFragmentByTag("fragment_commission");
                if (frag != null) {
                    manager.beginTransaction().remove(frag).commit();
                }
                else
                {
                    CommissionPopupFragment commissionFragment = new CommissionPopupFragment();
                    Bundle bundle = new Bundle();
                    bundle.putDouble("buyer_comm",d.getCurrentHomeBuyerCommPerc());
                    bundle.putDouble("seller_comm",d.getCurrentHomeSellerCommPerc());
                    commissionFragment.setArguments(bundle);
                    commissionFragment.show(manager, "fragment_commission");
                }
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finishCurrHomeActivity();
            }
        });

        final EditText salePriceEdit = (EditText)findViewById(R.id.salePriceEdit);
        salePriceEdit.setSelectAllOnFocus(true);
        salePriceEdit.setShowSoftInputOnFocus(true);
        salePriceEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                d.setCurrentHomeSalePrice(Double.parseDouble(salePriceEdit.getText().toString()));
                d.calcDeal();
                setData();
                return false;
            }
        });
        salePriceEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    d.setCurrentHomeSalePrice(Double.parseDouble(salePriceEdit.getText().toString()));
                    d.calcDeal();
                    setData();
                }
            }
        });
        final EditText closingCostEdit = (EditText)findViewById(R.id.closingCostEdit);
        closingCostEdit.setSelectAllOnFocus(true);
        closingCostEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                d.setCurrentHomeClosingCost(Double.parseDouble(closingCostEdit.getText().toString()));
                d.calcDeal();
                setData();
                return false;
            }
        });
        closingCostEdit.setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v,boolean hasFocus){
                if (!hasFocus){
                    d.setCurrentHomeClosingCost(Double.parseDouble(closingCostEdit.getText().toString()));
                    d.calcDeal();
                    setData();
                }
            }
        });
        final EditText totalOwedEdit = (EditText)findViewById(R.id.totalOwedEdit);
        totalOwedEdit.setSelectAllOnFocus(true);
        totalOwedEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                d.setCurrentHomeTotalOwed(Double.parseDouble(totalOwedEdit.getText().toString()));
                d.calcDeal();
                setData();
                return false;
            }
        });

        totalOwedEdit.setOnFocusChangeListener(new TextView.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v,boolean hasFocus){
                if (!hasFocus){
                    d.setCurrentHomeTotalOwed(Double.parseDouble(totalOwedEdit.getText().toString()));
                    d.calcDeal();
                    setData();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_realtool, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                FragmentManager manager = getFragmentManager();
                Fragment frag = manager.findFragmentByTag("fragment_settings");
                if (frag != null) {
                    manager.beginTransaction().remove(frag).commit();
                } else {
                    SettingsFragment settingsFragment = new SettingsFragment();
                    settingsFragment.show(manager, "fragment_settings");
                    return true;
                }
            }
        return super.onOptionsItemSelected(item);
    }

    public void finishCurrHomeActivity()
    {
        if (curr_deal_id == 0)
           curr_deal_id = DealToolFileHelper.insertNewDeal(this,d);
        else
           DealToolFileHelper.updateCurrentDeal(d,d.getDealID(),this);

        Bundle resultData = new Bundle();
        resultData.putString("START_NEW_HOME","TRUE");
        resultData.putLong("CURR_DEAL_ID",curr_deal_id);
        Intent intent = new Intent();
        intent.putExtras(resultData);
        setResult(RESULT_OK,intent);
        finish();
    }

    public void setData()
    {

        TextView netProceedsText = (TextView)findViewById(R.id.netProceedsText);
        TextView totalDownText = (TextView)findViewById(R.id.totalDownText);
        EditText salePrice = (EditText)findViewById(R.id.salePriceEdit);
        EditText closingCost = (EditText)findViewById(R.id.closingCostEdit);
        EditText totalOwed = (EditText)findViewById(R.id.totalOwedEdit);

        salePrice.setText(String.format("%1.2f",d.getCurrentHomeSalePrice()));
        closingCost.setText(String.format("%1.2f",d.getCurrentHomeClosingCost()));
        totalOwed.setText(String.format("%1.2f",d.getCurrentHomeTotalOwed()));
        netProceedsText.setText(String.format("%1.2f",d.getCurrentHomeNetProceeds()));
        totalDownText.setText(String.format("%1.2f", d.getCurrentHomeTotalDown()));
    }

    @Override
    public void onCommissionPercentSet(double sellerCommissionPercent, double buyerCommissionPercent) {
        d.setCurrentHomeBuyerCommPerc(buyerCommissionPercent/100);
        d.setCurrentHomeSellerCommPerc(sellerCommissionPercent/100);

        d.calcDeal();

        setData();

    }
}
