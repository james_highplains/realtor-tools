package com.jsoft.realtools;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DialogFragment;

import android.os.Build;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * Created by James on 12/14/2015.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CommissionPopupFragment extends DialogFragment implements View.OnClickListener{

    OnCommissionPercentSet mCallback;

    private Button okButton;
    private EditText sellerCommissionPercent;
    private EditText buyerCommissionPercent;


    public interface OnCommissionPercentSet
    {
        public void onCommissionPercentSet(double sellerCommissionPercent, double buyerCommissionPercent);
    }


        @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try{
            mCallback = (OnCommissionPercentSet) activity;
        }
        catch(ClassCastException e)
        {
            throw new ClassCastException(activity.toString()+ " must implement OnCommissionPercentSet");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_commission_popup, container);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        sellerCommissionPercent = (EditText)view.findViewById(R.id.sellerAgentText);
        sellerCommissionPercent.setText(String.format("%1.2f",getArguments().getDouble("seller_comm")*100));
        sellerCommissionPercent.setSelectAllOnFocus(true);

        buyerCommissionPercent = (EditText)view.findViewById(R.id.buyerAgentEdit);
        buyerCommissionPercent.setText(String.format("%1.2f",getArguments().getDouble("buyer_comm")*100));
        buyerCommissionPercent.setSelectAllOnFocus(true);
        buyerCommissionPercent.setShowSoftInputOnFocus(true);

        okButton = (Button)view.findViewById(R.id.okButton);

        getDialog().setTitle("Enter Commission Percentages");
        okButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v)
    {
        Editable buyerCommPerc = buyerCommissionPercent.getText();
        Editable sellerCommPerc = sellerCommissionPercent.getText();

        double buyer = 0.0;
        double seller = 0.0;

        if (!TextUtils.isEmpty(buyerCommPerc))
        {
            buyer = Double.parseDouble(buyerCommPerc.toString());
        }

        if (!TextUtils.isEmpty(sellerCommPerc))
        {
            seller = Double.parseDouble(sellerCommPerc.toString());
        }

        mCallback.onCommissionPercentSet(seller,buyer);
        this.dismiss();
    }


}
