package com.jsoft.realtools;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Layout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.jsoft.realtools.R;

/**
 * Created by James on 9/6/2015.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class NameDealFragment extends DialogFragment implements  View.OnClickListener {
    private EditText dealName;
    private Button okButton;

    public interface OnDealNameSet
    {
        void onDealNameSet (String dealName);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
         View view = inflater.inflate(R.layout.name_deal_fragment, container);
         dealName = (EditText) view.findViewById(R.id.newDealname);
         okButton = (Button) view.findViewById(R.id.nameDealFragOKButton);

         getDialog().setTitle("Enter Deal Name");
         okButton.setOnClickListener(this);
         return view;
    }

    @Override
    public void onClick(View v)
    {
        Editable dealNameText = dealName.getText();
        if (!TextUtils.isEmpty(dealNameText))
        {
            DealSummaryActivity activity =  (DealSummaryActivity) getActivity();
            activity.onDealNameSet(dealNameText.toString());
            this.dismiss();
        }
    }
}
