package com.jsoft.realtools;

import android.provider.BaseColumns;

/**
 * Created by James on 7/21/2016.
 */
public class RealtorToolsContract {
    public RealtorToolsContract() {}

    public static abstract class DealTable implements BaseColumns{
        public static final String TABLE_NAME = "deal";
        public static final String COLUMN_NAME_DEAL_NAME = "dealName";
        public static final String COLUMN_NAME_CURR_HOME_SALE_PRICE = "currentHomeSalePrice";
        public static final String COLUMN_NAME_CURR_HOME_CLOSING_COST = "currentHomeClosingCost";
        public static final String COLUMN_NAME_CURR_HOME_SELLER_COMM_PERC = "currentHomeSellerCommPerc";
        public static final String COLUMN_NAME_CURR_HOME_SELLER_COMM = "currentHomeSellerComm";
        public static final String COLUMN_NAME_CURR_HOME_BUYER_COMM = "currentHomeBuyerComm";
        public static final String COLUMN_NAME_CURR_HOME_BUYER_COMM_PERC = "currentHomeBuyerCommPerc";
        public static final String COLUMN_NAME_CURR_HOME_TOTAL_OWED = "currentHomeTotalOwed";
        public static final String COLUMN_NAME_CURR_HOME_ADD_CASH = "currentHomeAdditionalCash";
        public static final String COLUMN_NAME_CURR_HOME_NET_PROCEEDS = "currentHomeNetProceeds";
        public static final String COLUMN_NAME_CURR_HOME_TOTAL_DOWN = "currentHomeTotalDown";
        public static final String COLUMN_NAME_NEW_HOME_SALE_PRICE = "newHomeSalePrice";
        public static final String COLUMN_NAME_NEW_HOME_SELLER_COMM_PERC = "newHomeSellerCommPerc";
        public static final String COLUMN_NAME_NEW_HOME_BUYER_COMM_PERC = "newHomeBuyerCommPerc";
        public static final String COLUMN_NAME_NEW_HOME_SELLER_COMM = "newHomeSellerComm";
        public static final String COLUMN_NAME_NEW_HOME_BUYER_COMM = "newHomeBuyerComm";
        public static final String COLUMN_NAME_NEW_HOME_CLOSING_COSTS = "newHomeClosingCosts";
        public static final String COLUMN_NAME_NEW_HOME_MORT_AMOUNT = "newHomeMortgageAmount";
        public static final String COLUMN_NAME_NEW_HOME_LTV = "newHomeLTV";
        public static final String COLUMN_NAME_ACTIVE = "active";
        public static final String COLUMN_NAME_NEW_HOME_EST_TAXES = "estimatedTaxes";
        public static final String COLUMN_NAME_NEW_HOME_EST_INS = "estimatedInsurance";
        public static final String COLUMN_NAME_NEW_HOME_APR = "newHomeAPR";
        public static final String COLUMN_NAME_NEW_HOME_PI = "newHomePI";
        public static final String COLUMN_NAME_NEW_HOME_PITI = "newHomePITI";
        public static final String COLUMN_NAME_NEW_HOME_TERM = "newHomeTerm";
        public static final String COLUMN_NAME_PRIMARY_CONTACT_ID = "primaryContactID";
        public static final String COLUMN_NAME_ALT_CONTACT_ID = "alternateContactID";
    }

    public static abstract class ContactTable implements BaseColumns{
        public static final String TABLE_NAME = "rtcontacts";
        public static final String COLUMN_NAME_FIRST_NAME = "firstName";
        public static final String COLUMN_NAME_LAST_NAME = "lastName";
        public static final String COLUMN_NAME_PHONE1 = "phone1";
        public static final String COLUMN_NAME_PHONE1_TYPE = "phone1Type";
        public static final String COLUMN_NAME_PHONE2 = "phone2";
        public static final String COLUMN_NAME_PHONE2_TYPE = "phone2Type";
        public static final String COLUMN_NAME_EMAIL = "email";
    }

    public static abstract class SettingsTable implements BaseColumns{
        public static final String TABLE_NAME = "settings";
        public static final String COLUMN_NAME_SETTING_CODE = "code";
        public static final String COLUMN_NAME_SETTING_VALUE = "settingValue";
        public static final String COLUMN_NAME_SETTING_BLOB = "settingBlob";
    }

}
