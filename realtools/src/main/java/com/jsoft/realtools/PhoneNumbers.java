package com.jsoft.realtools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by James on 8/17/2016.
 */
public class PhoneNumbers {
    /**
     * An array of sample (dummy) items.
     */
    public static final List<PhoneNumber> ITEMS = new ArrayList<PhoneNumber>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, PhoneNumber> ITEM_MAP = new HashMap<String, PhoneNumber>();

    private static final int COUNT = 0;


    public static void addNumber(PhoneNumber number) {
        ITEMS.add(number);
        ITEM_MAP.put(number.id, number);
    }


    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class PhoneNumber {
        public final String id;
        public final String type;
        public final String number;

        public PhoneNumber(String id, String type, String number) {
            this.id = id;
            this.type = type;
            this.number = number;
        }

        public String getType()
        {
            return type;
        }

        public String getNumber()
        {
            return number;
        }

        @Override
        public String toString() {
            return number;
        }
    }
}
