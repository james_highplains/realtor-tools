package com.jsoft.realtools;

/**
 * Created by James on 7/16/2016.
 */
public class rtSettings implements java.io.Serializable
{
    private String agentName="";
    private String agentPhone="";
    private String agentEmail = "";
    private String agentTextLine1 = "";
    private String agentTextLine2 = "";
    private String agentTextLine3 = "";

    //These are defaults in CO.  May be different in other locales.  Not sure
    private Double defaultBuyCommPercent = .028;
    private Double defaultSellCommPercent = .032;
    private Double defaultAPR = .05;
    private Double defaultClosingCostPercent = .01;
    private Double defaultClosingCostAmount = 0.0;
    private String agentPhoto="";

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentPhone() {
        return agentPhone;
    }

    public void setAgentPhone(String agentPhone) {
        this.agentPhone = agentPhone;
    }

    public Double getDefaultBuyCommPercent() {
        return defaultBuyCommPercent;
    }

    public void setDefaultBuyCommPercent(Double defaultBuyCommPercent) {
        this.defaultBuyCommPercent = defaultBuyCommPercent;
    }

    public Double getDefaultSellCommPercent() {
        return defaultSellCommPercent;
    }

    public void setDefaultSellCommPercent(Double defaultSellCommPercent) {
        this.defaultSellCommPercent = defaultSellCommPercent;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public String getAgentTextLine1() {
        return agentTextLine1;
    }

    public void setAgentTextLine1(String agentTextLine1) {
        this.agentTextLine1 = agentTextLine1;
    }

    public String getAgentTextLine2() {
        return agentTextLine2;
    }

    public void setAgentTextLine2(String agentTextLine2) {
        this.agentTextLine2 = agentTextLine2;
    }

    public String getAgentTextLine3() {
        return agentTextLine3;
    }

    public void setAgentTextLine3(String agentTextLine3) {
        this.agentTextLine3 = agentTextLine3;
    }

    public Double getDefaultAPR() {
        return defaultAPR;
    }

    public void setDefaultAPR(Double defaultAPR) {
        this.defaultAPR = defaultAPR;
    }

    public Double getDefaultClosingCostPercent() {
        return defaultClosingCostPercent;
    }

    public void setDefaultClosingCostPercent(Double defaultClosingCostPercent) {
        this.defaultClosingCostPercent = defaultClosingCostPercent;
    }

    public Double getDefaultClosingCostAmount() {
        return defaultClosingCostAmount;
    }

    public void setDefaultClosingCostAmount(Double defaultClosingCostAmount) {
        this.defaultClosingCostAmount = defaultClosingCostAmount;
    }

    public String getAgentPhoto() {
        return agentPhoto;
    }

    public void setAgentPhoto(String agentPhoto) {
        this.agentPhoto = agentPhoto;
    }
}
