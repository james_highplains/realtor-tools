package com.jsoft.realtools;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;

import android.os.Bundle;
import android.os.Environment;

import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class DealSummaryActivity extends Activity {

    Deal curr_deal;
    boolean start_mode_summary = false;
    long curr_deal_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_summary);

        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            start_mode_summary = extras.getBoolean("START_MODE");
            curr_deal_id = extras.getLong("CURR_DEAL_ID");
        }

        if (start_mode_summary)
        {
            curr_deal = DealToolFileHelper.getCurrentDeal(curr_deal_id,this);
            setData();

        }
        else
        {
            curr_deal = new Deal();
            DealToolFileHelper.applyDealDefaults(this,curr_deal);
            setData();
            startNewDeal();
        }

        final Button cancelButton = (Button) findViewById(R.id.cancelButton);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelSummaryView();
            }
        });

        final Button newHomeButton = (Button) findViewById(R.id.newHomeButton);
        newHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startNewHomeActivity();
            }
        });

        final Button currHomeButton = (Button)findViewById(R.id.currHomeButton);
        currHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCurrHomeActivity();
            }
        });

        final Button commissionButton = (Button)findViewById(R.id.commissionButton);
        commissionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                showCommissionSummary();
            }
        });

        final Button contactButton = (Button)findViewById(R.id.addContactsButton);
        contactButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startContactFragment();
            }
        });
    }

    private void startContactFragment()
    {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag("fragment_contact");
        if (frag != null)
        {
            manager.beginTransaction().remove(frag).commit();
        }
        else
        {
            ContactFragment contactFrag = new ContactFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("CURR_DEAL_ID",curr_deal_id);
            contactFrag.setArguments(bundle);
            contactFrag.show(manager, "fragment_contact");
        }
    }
    private void startCurrHomeActivity()
    {
        Intent intent = new Intent(this,CurrHomeActivity.class);
        DealToolFileHelper.saveCurrentDeal(this,curr_deal);
        intent.putExtra("CURR_DEAL_ID",curr_deal_id);
        startActivityForResult(intent,100);
    }

    private void startNewHomeActivity()
    {
        Intent intent = new Intent(this, NewHomeActivity.class);
        DealToolFileHelper.saveCurrentDeal(this,curr_deal);
        intent.putExtra("CURR_DEAL_ID",curr_deal_id);
        startActivityForResult(intent,101);

    }

    private void cancelSummaryView()
    {
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100){
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                curr_deal_id = extras.getLong("CURR_DEAL_ID");
                curr_deal = DealToolFileHelper.getCurrentDeal(curr_deal_id, this);

                if (curr_deal.getDealName() == null || curr_deal.getDealName().equalsIgnoreCase("Unnamed Deal")) {
                    FragmentManager manager = getFragmentManager();
                    Fragment frag = manager.findFragmentByTag("fragment_name_deal");
                    if (frag != null) {
                        manager.beginTransaction().remove(frag).commit();
                    } else {
                        NameDealFragment nameDealFragment = new NameDealFragment();
                        nameDealFragment.show(manager, "fragment_name_deal");
                    }
                }
            }
        } else if (requestCode == 101)
        {
            curr_deal = DealToolFileHelper.getCurrentDeal(curr_deal_id,this);
        }


        setData();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void showCommissionSummary()
    {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag("fragment_commission_summary");
        if (frag != null)
        {
            manager.beginTransaction().remove(frag).commit();
        }
        else
        {
            CommissionSummaryFragment commSummFrag = new CommissionSummaryFragment();
            Bundle bundle = new Bundle();
            bundle.putLong("CURR_DEAL_ID",curr_deal_id);
            commSummFrag.setArguments(bundle);
            commSummFrag.show(manager, "fragment_commission_summary");
        }
    }

    private void setData()
    {
        TextView summSalePriceText = (TextView) findViewById(R.id.summSalePriceText);
        TextView summClosingCostText = (TextView) findViewById(R.id.summClosingCostText);
        TextView summTotalCommText = (TextView) findViewById(R.id.summTotalCommText);
        TextView summAddCashText = (TextView) findViewById(R.id.summAddCashText);
        TextView summAmountOwedText = (TextView) findViewById(R.id.summAmountOwedText);
        TextView summTotalDownText = (TextView) findViewById(R.id.summTotalDownText);
        TextView summNewSalePriceText = (TextView) findViewById(R.id.summNewSalePrice);
        TextView summNewMortText = (TextView)findViewById(R.id.summNewMortText);
        TextView summNewMortLTV = (TextView)findViewById(R.id.summNewLTVText);
        TextView summNewEstPayment= (TextView)findViewById(R.id.summNewEstPayment);

        summSalePriceText.setText(String.format("%1$,.2f", curr_deal.getCurrentHomeSalePrice()));
        summClosingCostText.setText(String.format("%1$,.2f",curr_deal.getCurrentHomeClosingCost()));
        summTotalCommText.setText(String.format("%1$,.2f",curr_deal.getCurrentHomeBuyerComm() + curr_deal.getCurrentHomeSellerComm()));
        summAddCashText.setText(String.format("%1$,.2f",curr_deal.getCurrentHomeAdditionalCash()));
        summAmountOwedText.setText(String.format("%1$,.2f", curr_deal.getCurrentHomeTotalOwed()));
        summTotalDownText.setText(String.format("%1$,.2f", curr_deal.getCurrentHomeTotalDown()));
        if (curr_deal.getNewHomeSalePrice()>0)
            summNewSalePriceText.setText(String.format("%1$,.2f", curr_deal.getNewHomeSalePrice()));
        else
            summNewSalePriceText.setText("$0.00");

        if (curr_deal.getNewHomeMortgageAmount()>0)
            summNewMortText.setText(String.format("%1$,.2f", curr_deal.getNewHomeMortgageAmount()));
        else
            summNewMortText.setText("$0.00");

        if (curr_deal.getNewHomeLTV()>0)
            summNewMortLTV.setText(String.format("%3.2f", curr_deal.getNewHomeLTV() * 100));
        else
            summNewMortLTV.setText("0");

        if (curr_deal.getNewHomePITI()>0)
            summNewEstPayment.setText(String.format("%1$,.2f",curr_deal.getNewHomePITI()));
        else
            summNewEstPayment.setText("$0.00");
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void startRealToolActivity()
    {
        if (!start_mode_summary) {
            FragmentManager manager = getFragmentManager();
            Fragment frag = manager.findFragmentByTag("fragment_name_deal");
            if (frag != null) {
                manager.beginTransaction().remove(frag).commit();
            } else {
                NameDealFragment nameDealFragment = new NameDealFragment();
                nameDealFragment.show(manager, "fragment_name_deal");
            }
        }
        else {
            //TODO: Save the curr_deal back to the list
            DealToolFileHelper.updateCurrentDeal(curr_deal,curr_deal_id,this);

            finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_deal_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_settings: {
                FragmentManager manager = getFragmentManager();
                Fragment frag = manager.findFragmentByTag("fragment_settings");
                if (frag != null) {
                    manager.beginTransaction().remove(frag).commit();
                } else {
                    SettingsFragment settingsFragment = new SettingsFragment();
                    settingsFragment.show(manager, "fragment_settings");
                    return true;
                }
            }
            break;
            case R.id.action_send_summary:
                sendSummary();
                break;
            default:
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendSummary()
    {
        String primaryName = "";
        String secondaryName = "";
        String primaryEmail = "";
        String secondaryEmail = "";
        String contactId = "";

        rtSettings settings = DealToolFileHelper.getSettings(this);

        if (!curr_deal.getPrimaryContact().isEmpty())
        {
            Uri primaryContactUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, curr_deal.getPrimaryContact());
            Uri res = ContactsContract.Contacts.lookupContact(getContentResolver(),primaryContactUri);
            Cursor c = getContentResolver().query(res,null,null,null,null);
            //TODO: Get the contact email

            if (c.moveToFirst()){
                primaryName = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
            }
            Cursor emails = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,null,ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",new String[] {contactId},null);
            if (emails.moveToFirst()){
                primaryEmail = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
            }
            emails.close();
            c.close();
        }
        if (!curr_deal.getAlternateContact().isEmpty())
        {
            Uri secondaryContactUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, curr_deal.getAlternateContact());
            Uri res = ContactsContract.Contacts.lookupContact(getContentResolver(),secondaryContactUri);
            Cursor c = getContentResolver().query(res,null,null,null,null);
            //TODO: Get the contact email

            if (c.moveToFirst()){
                secondaryName = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
            }
            Cursor emails = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,null,ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",new String[] {contactId},null);
            if (emails.moveToFirst()){
                secondaryEmail = emails.getString(emails.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
            }
            emails.close();
            c.close();
        }

        String emailText = "Below you will find a summary of the deal we just worked on.  Please let me know if you have any questions.\n\n";
        emailText += "Sell\n";
        emailText += "\tSale Price: "+String.format("$%,.2f",curr_deal.getCurrentHomeSalePrice())+"\n";
        emailText += "\tClosing Costs: "+String.format("$%,.2f",curr_deal.getCurrentHomeClosingCost())+"\n";
        emailText += "\tAmount Owed: "+String.format("$%,.2f",curr_deal.getCurrentHomeTotalOwed())+"\n";
        emailText += "\tTotal Commissions: "+String.format("$%,.2f",curr_deal.getCurrentHomeSellerComm()+curr_deal.getCurrentHomeBuyerComm())+"\n";
        emailText += "\tAdditional Cash: "+String.format("$%,.2f",curr_deal.getCurrentHomeAdditionalCash())+"\n";
        emailText += "\tTotal Downpayment: "+String.format("$%,.2f",curr_deal.getCurrentHomeNetProceeds())+"\n\n\n\n";
        emailText += "Buy\n";
        emailText += "\tSale Price: "+String.format("$%,.2f",curr_deal.getNewHomeSalePrice())+"\n";
        emailText += "\tNew Mortgage Amount: "+String.format("$%,.2f",curr_deal.getNewHomeMortgageAmount())+"\n";
        emailText += "\tMortgage LTV: "+String.format("%%%3.2f",curr_deal.getNewHomeLTV()*100)+"\n";
        emailText += "\tEstimated Taxes: "+String.format("$%,.2f",curr_deal.getNewHomeEstTaxes())+"\n";
        emailText += "\tEstimated Insurance: "+String.format("$%,.2f",curr_deal.getNewHomeEstInsurance())+"\n";
        emailText += "\tEstimated APR: "+String.format("%%%3.2f",curr_deal.getNewHomeAPR()*100)+"\n";
        emailText += "\tEstimated Payment: "+String.format("$%,.2f",curr_deal.getNewHomePITI())+"\n\n\n";
        emailText += "This summary is for informational purposes only and is not intended to be construed as an offer or a contract for any specific property.  Estimated payment amount is not a guarantee of qualification for financing and a mortgage lender should be consulted for actual rates and payment terms.";

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String[] sendTo = new String[2];
        if (!primaryEmail.isEmpty())
            sendTo[0] = primaryEmail;
        if (!secondaryEmail.isEmpty())
            sendTo[1] = secondaryEmail;

        intent.putExtra(Intent.EXTRA_EMAIL,sendTo);
        intent.putExtra(Intent.EXTRA_SUBJECT,"Realtor Tools Deal Summary");
        intent.putExtra(Intent.EXTRA_TEXT,emailText);

        startActivity(intent);    }
    public void onDealNameSet(String dealName)
    {
        curr_deal.setDealName(dealName);
        DealToolFileHelper.updateCurrentDeal(curr_deal,curr_deal_id,this);
        //this.finish();
    }

    public void startNewDeal() {
        Intent intent = new Intent(this, CurrHomeActivity.class);
        //Put in code to indicate new deal
        intent.putExtra("CURR_DEAL_ID",curr_deal_id);
        //Deal d = new Deal();
        //DealToolFileHelper.saveCurrentDeal(this, d);
        startActivityForResult(intent, 100);

    }

}
