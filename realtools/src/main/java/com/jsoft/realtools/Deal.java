package com.jsoft.realtools;

/**
 * Created by James on 8/29/2015.
 */
public class Deal implements java.io.Serializable {
    private long dealID = 0;
    private double currentHomeSalePrice= 0.0;
    private double currentHomeClosingCost = 0.0;
    private double currentHomeSellerCommPerc;
    private double currentHomeSellerComm = 0.0;
    private double currentHomeBuyerComm = 0.0;
    private double currentHomeBuyerCommPerc;
    private double currentHomeTotalOwed = 0.0;
    private double currentHomeAdditionalCash = 0.0;
    private double currentHomeNetProceeds = 0.0;
    private double currentHomeTotalDown = 0.0;

    private double newHomeSalePrice = 0.0;
    private double newHomeSellerCommPerc;
    private double newHomeBuyerCommPerc;
    private double newHomeSellerComm = 0.0;
    private double newHomeBuyerComm = 0.0;
    private double newHomeClosingCosts = 0.0;
    private double newHomeMortgageAmount = 0.0;
    private double newHomeLTV = 0.0;
    private double newHomeAPR = 0.0;
    private long newHomeTerm = 30;
    private double newHomeEstTaxes = 0.0;
    private double newHomeEstInsurance = 0.0;
    private double newHomePI = 0.0;
    private double newHomePITI = 0.0;

    private String dealName = "Unnamed Deal";

    private String primaryContact="";
    private String alternateContact="";

    private boolean active = true;

    public Deal()
    {
    }

    public long getDealID(){ return dealID;}
    public void setDealID(long id)
    {
        this.dealID = id;
    }


    public double getCurrentHomeSalePrice() {
        return currentHomeSalePrice;
    }

    public void setCurrentHomeSalePrice(double currentHomeSalePrice) {
        this.currentHomeSalePrice = currentHomeSalePrice;
    }

    public double getCurrentHomeClosingCost() {
        return currentHomeClosingCost;
    }

    public void setCurrentHomeClosingCost(double currentHomeClosingCost) {
        this.currentHomeClosingCost = currentHomeClosingCost;
    }

    public double getCurrentHomeSellerCommPerc() {
        return currentHomeSellerCommPerc;
    }

    public void setCurrentHomeSellerCommPerc(double currentHomeSellerCommPerc) {
        this.currentHomeSellerCommPerc = currentHomeSellerCommPerc;
    }

    public double getCurrentHomeBuyerCommPerc() {
        return currentHomeBuyerCommPerc;
    }

    public void setCurrentHomeBuyerCommPerc(double currentHomeBuyerCommPerc) {
        this.currentHomeBuyerCommPerc = currentHomeBuyerCommPerc;
    }

    public double getCurrentHomeTotalOwed() {
        return currentHomeTotalOwed;
    }

    public void setCurrentHomeTotalOwed(double currentHomeTotalOwed) {
        this.currentHomeTotalOwed = currentHomeTotalOwed;
    }

    public double getCurrentHomeAdditionalCash() {
        return currentHomeAdditionalCash;
    }

    public void setCurrentHomeAdditionalCash(double currentHomeAdditionalCash) {
        this.currentHomeAdditionalCash = currentHomeAdditionalCash;
    }

    public double getNewHomeSalePrice() {
        return newHomeSalePrice;
    }

    public void setNewHomeSalePrice(double newHomeSalePrice) {
        this.newHomeSalePrice = newHomeSalePrice;
    }

    public double getNewHomeSellerCommPerc() {
        return newHomeSellerCommPerc;
    }

    public void setNewHomeSellerCommPerc(double newHomeSellerCommPerc) {
        this.newHomeSellerCommPerc = newHomeSellerCommPerc;
    }

    public double getNewHomeBuyerCommPerc() {
        return newHomeBuyerCommPerc;
    }

    public void setNewHomeBuyerCommPerc(double newHomeBuyerCommPerc) {
        this.newHomeBuyerCommPerc = newHomeBuyerCommPerc;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public String getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
    }

    public String getAlternateContact() {
        return alternateContact;
    }

    public void setAlternateContact(String alternateContact) {
        this.alternateContact = alternateContact;
    }


    public double getCurrentHomeSellerComm() {
        return currentHomeSellerComm;
    }

    public void setCurrentHomeSellerComm(double currentHomeSellerComm) {
        this.currentHomeSellerComm = currentHomeSellerComm;
    }

    public double getCurrentHomeBuyerComm() {
        return currentHomeBuyerComm;
    }

    public void setCurrentHomeBuyerComm(double currentHomeBuyerComm) {
        this.currentHomeBuyerComm = currentHomeBuyerComm;
    }

    public double getNewHomeSellerComm() {
        return newHomeSellerComm;
    }

    public void setNewHomeSellerComm(double newHomeSellerComm) {
        this.newHomeSellerComm = newHomeSellerComm;
    }

    public double getNewHomeBuyerComm() {
        return newHomeBuyerComm;
    }

    public void setNewHomeBuyerComm(double newHomeBuyerComm) {
        this.newHomeBuyerComm = newHomeBuyerComm;
    }

    public void calcCurrentHome(){
        setCurrentHomeSellerComm(currentHomeSalePrice * currentHomeSellerCommPerc);
        setCurrentHomeBuyerComm(currentHomeSalePrice * currentHomeBuyerCommPerc);
        setCurrentHomeNetProceeds(currentHomeSalePrice - currentHomeClosingCost-currentHomeSellerComm - currentHomeBuyerComm - currentHomeTotalOwed);
        setCurrentHomeTotalDown(currentHomeNetProceeds+currentHomeAdditionalCash);

    }

    public double getCurrentHomeNetProceeds() {
        return currentHomeNetProceeds;
    }

    public void setCurrentHomeNetProceeds(double currentHomeNetProceeds) {
        this.currentHomeNetProceeds = currentHomeNetProceeds;
    }

    public double getCurrentHomeTotalDown() {
        return currentHomeTotalDown;
    }

    public void setCurrentHomeTotalDown(double currentHomeTotalDown) {
        this.currentHomeTotalDown = currentHomeTotalDown;
    }


    public double getNewHomeMortgageAmount() {
        return newHomeMortgageAmount;
    }

    public void setNewHomeMortgageAmount(double newHomeMortgageAmount) {
        this.newHomeMortgageAmount = newHomeMortgageAmount;
    }

    public void calcNewHome(){
        double APR = getNewHomeAPR()/12;
        double term = getNewHomeTerm()*12;

        double loanAmount = getNewHomeMortgageAmount();
        double payment = 0.0;

        setNewHomeBuyerComm(newHomeSalePrice*newHomeBuyerCommPerc);
        setNewHomeSellerComm(newHomeSalePrice*newHomeSellerCommPerc);
        setNewHomeMortgageAmount((newHomeSalePrice+newHomeClosingCosts)-currentHomeTotalDown);
        setNewHomeLTV(newHomeMortgageAmount/newHomeSalePrice);
        payment = (APR*loanAmount*(Math.pow(1+APR,term)))/(Math.pow(1+APR,term)-1);
        setNewHomePI(payment);
        setNewHomePITI(payment+((getNewHomeEstInsurance()+getNewHomeEstTaxes())/12));

    }

    public void calcDeal(){
        calcCurrentHome();
        calcNewHome();
    }
    public double getNewHomeClosingCosts() {
        return newHomeClosingCosts;
    }

    public void setNewHomeClosingCosts(double newHomeClosingCosts) {
        this.newHomeClosingCosts = newHomeClosingCosts;
    }

    public double getNewHomeLTV() {
        return newHomeLTV;
    }

    public void setNewHomeLTV(double newHomeLTV) {
        this.newHomeLTV = newHomeLTV;
    }

    public String toString()
    {
        String retVal = this.getDealName();
        if (retVal ==null)
            return "Unnamed Deal";
        else if (retVal.length()==0)
               return "Unnamed Deal";
            else
               return retVal;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    public double getNewHomeEstTaxes() {
        return newHomeEstTaxes;
    }

    public void setNewHomeEstTaxes(double newHomeEstTaxes) {
        this.newHomeEstTaxes = newHomeEstTaxes;
    }

    public double getNewHomeEstInsurance() {
        return newHomeEstInsurance;
    }

    public void setNewHomeEstInsurance(double newHomeEstInsurance) {
        this.newHomeEstInsurance = newHomeEstInsurance;
    }

    public double getNewHomePI() {
        return newHomePI;
    }

    public void setNewHomePI(double newHomePI) {
        this.newHomePI = newHomePI;
    }

    public double getNewHomePITI() {
        return newHomePITI;
    }

    public void setNewHomePITI(double newHomePITI) {
        this.newHomePITI = newHomePITI;
    }

    public double getNewHomeAPR() {
        return newHomeAPR;
    }

    public void setNewHomeAPR(double newHomeAPR) {
        this.newHomeAPR = newHomeAPR;
    }

    public long getNewHomeTerm() {
        return newHomeTerm;
    }

    public void setNewHomeTerm(long newHomeTerm) {
        this.newHomeTerm = newHomeTerm;
    }
}
