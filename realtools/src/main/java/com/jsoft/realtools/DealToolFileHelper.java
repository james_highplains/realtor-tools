package com.jsoft.realtools;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;

import com.jsoft.realtools.Deal;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by James on 9/2/2015.
 */
public abstract class DealToolFileHelper
{
    public static Deal getCurrentDeal(long dealID, Context context)
    {
        Deal d = new Deal();
        String[] columns = {
                RealtorToolsContract.DealTable._ID,
                RealtorToolsContract.DealTable.COLUMN_NAME_DEAL_NAME,
                RealtorToolsContract.DealTable.COLUMN_NAME_ACTIVE,
                RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SALE_PRICE,
                RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_ADD_CASH,
                RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_CLOSING_COST,
                RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_BUYER_COMM_PERC,
                RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_BUYER_COMM,
                RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SELLER_COMM_PERC,
                RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SELLER_COMM,
                RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_TOTAL_OWED,
                RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_NET_PROCEEDS,
                RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_TOTAL_DOWN,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SALE_PRICE,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_CLOSING_COSTS,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_MORT_AMOUNT,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_LTV,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_BUYER_COMM_PERC,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_BUYER_COMM,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SELLER_COMM_PERC,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SELLER_COMM,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_EST_TAXES,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_EST_INS,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_APR,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_PI,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_PITI,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_TERM,
                RealtorToolsContract.DealTable.COLUMN_NAME_PRIMARY_CONTACT_ID,
                RealtorToolsContract.DealTable.COLUMN_NAME_ALT_CONTACT_ID,
        };
        String selection = "_ID = ?";
        String[] selectionArgs = {Long.valueOf(dealID).toString()};

        RealToolsDbHelper helper = new RealToolsDbHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();

        Cursor c = db.query(
                RealtorToolsContract.DealTable.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        try{
            if (c.moveToFirst()) {
                d.setDealID(c.getInt(0));
                d.setDealName(c.getString(1));
                if (c.getString(2).equals("Y"))
                    d.setActive(true);
                else
                    d.setActive(false);
                d.setCurrentHomeSalePrice(c.getDouble(3));
                d.setCurrentHomeAdditionalCash(c.getDouble(4));
                d.setCurrentHomeClosingCost(c.getDouble(5));
                d.setCurrentHomeBuyerCommPerc(c.getDouble(6));
                d.setCurrentHomeBuyerComm(c.getDouble(7));
                d.setCurrentHomeSellerCommPerc(c.getDouble(8));
                d.setCurrentHomeSellerComm(c.getDouble(9));
                d.setCurrentHomeTotalOwed(c.getDouble(10));
                d.setCurrentHomeNetProceeds(c.getDouble(11));
                d.setCurrentHomeTotalDown(c.getDouble(12));
                d.setNewHomeSalePrice(c.getDouble(13));
                d.setNewHomeClosingCosts(c.getDouble(14));
                d.setNewHomeMortgageAmount(c.getDouble(15));
                d.setNewHomeLTV(c.getDouble(16));
                d.setNewHomeBuyerCommPerc(c.getDouble(17));
                d.setNewHomeBuyerComm(c.getDouble(18));
                d.setNewHomeSellerCommPerc(c.getDouble(19));
                d.setNewHomeSellerComm(c.getDouble(20));
                d.setNewHomeEstTaxes(c.getDouble(21));
                d.setNewHomeEstInsurance(c.getDouble(22));
                d.setNewHomeAPR(c.getDouble(23));
                d.setNewHomePI(c.getDouble(24));
                d.setNewHomePITI(c.getDouble(25));
                d.setNewHomeTerm(c.getLong(26));
                d.setPrimaryContact(c.getString(27));
                d.setAlternateContact(c.getString(28));
            }
        }finally
        {
            c.close();
        }

        return d;
    }

    public static long insertNewDeal(Context context, Deal deal)
    {

        RealToolsDbHelper helper = new RealToolsDbHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();

        ContentValues values = new ContentValues();

        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_DEAL_NAME,deal.getDealName());
        if (deal.isActive())
          values.put(RealtorToolsContract.DealTable.COLUMN_NAME_ACTIVE,"Y");
        else
            values.put(RealtorToolsContract.DealTable.COLUMN_NAME_ACTIVE,"N");

        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SALE_PRICE,deal.getCurrentHomeSalePrice());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_ADD_CASH,deal.getCurrentHomeAdditionalCash());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_CLOSING_COST,deal.getCurrentHomeClosingCost());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_BUYER_COMM_PERC,deal.getCurrentHomeBuyerCommPerc());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_BUYER_COMM,deal.getCurrentHomeBuyerComm());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SELLER_COMM_PERC,deal.getCurrentHomeSellerCommPerc());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SELLER_COMM,deal.getCurrentHomeSellerComm());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_TOTAL_OWED,deal.getCurrentHomeTotalOwed());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_NET_PROCEEDS,deal.getCurrentHomeNetProceeds());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_TOTAL_DOWN,deal.getCurrentHomeTotalDown());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SALE_PRICE,deal.getNewHomeSalePrice());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_CLOSING_COSTS,deal.getNewHomeClosingCosts());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_MORT_AMOUNT,deal.getNewHomeMortgageAmount());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_LTV,deal.getNewHomeLTV());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_BUYER_COMM_PERC,deal.getNewHomeBuyerCommPerc());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_BUYER_COMM,deal.getNewHomeBuyerComm());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SELLER_COMM_PERC,deal.getNewHomeSellerCommPerc());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SELLER_COMM,deal.getNewHomeSellerComm());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_EST_TAXES,deal.getNewHomeEstTaxes());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_EST_INS,deal.getNewHomeEstInsurance());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_APR,deal.getNewHomeAPR());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_PI,deal.getNewHomePI());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_PITI,deal.getNewHomePITI());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_TERM,deal.getNewHomeTerm());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_PRIMARY_CONTACT_ID,deal.getPrimaryContact());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_ALT_CONTACT_ID,deal.getAlternateContact());

        long dealID = db.insert(RealtorToolsContract.DealTable.TABLE_NAME,
                null,
                values);

        return dealID;
    }

    public static void saveCurrentDeal(Context context, Deal deal)
    {

        String FILENAME = "curr_deal";
        FileOutputStream fos = null;
        try {
            fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(deal);
            out.flush();
            out.reset();
            out.close();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e){
        e.printStackTrace();}



    }

    public static void cleanCurrentDeal(Context context)
    {
        String FILENAME = "curr_deal";
        try
        {
            context.deleteFile(FILENAME);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ArrayList getAllCurrentDeals(Context context)
    {
        RealToolsDbHelper helper = new RealToolsDbHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        ArrayList list = new ArrayList();
        String[] columns = {
             RealtorToolsContract.DealTable._ID,
             RealtorToolsContract.DealTable.COLUMN_NAME_DEAL_NAME,
             RealtorToolsContract.DealTable.COLUMN_NAME_ACTIVE,
             RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SALE_PRICE,
             RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_ADD_CASH,
             RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_CLOSING_COST,
             RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_BUYER_COMM_PERC,
             RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_BUYER_COMM,
             RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SELLER_COMM_PERC,
             RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SELLER_COMM,
             RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_TOTAL_OWED,
             RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_NET_PROCEEDS,
             RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_TOTAL_DOWN,
             RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SALE_PRICE,
             RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_CLOSING_COSTS,
             RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_MORT_AMOUNT,
             RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_LTV,
             RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_BUYER_COMM_PERC,
             RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_BUYER_COMM,
             RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SELLER_COMM_PERC,
             RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SELLER_COMM,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_EST_TAXES,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_EST_INS,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_APR,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_PI,
                RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_PITI,
            RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_TERM,
                RealtorToolsContract.DealTable.COLUMN_NAME_PRIMARY_CONTACT_ID,
                RealtorToolsContract.DealTable.COLUMN_NAME_ALT_CONTACT_ID,
        };

        String selection = RealtorToolsContract.DealTable.COLUMN_NAME_ACTIVE + " = ?";

        String[] selectionArgs = {
                "Y",
        };
         Cursor c = db.query(
                 RealtorToolsContract.DealTable.TABLE_NAME,
                 columns,
                 selection,
                 selectionArgs,
                 null,
                 null,
                 null
         );

        try{
            int position =0;
            while (c.moveToNext()){
                Deal d = new Deal();
                d.setDealID(c.getLong(0));
                d.setDealName(c.getString(1));
                if (c.getString(2).equals("Y"))
                  d.setActive(true);
                else
                    d.setActive(false);
                d.setCurrentHomeSalePrice(c.getDouble(3));
                d.setCurrentHomeAdditionalCash(c.getDouble(4));
                d.setCurrentHomeClosingCost(c.getDouble(5));
                d.setCurrentHomeBuyerCommPerc(c.getDouble(6));
                d.setCurrentHomeBuyerComm(c.getDouble(7));
                d.setCurrentHomeSellerCommPerc(c.getDouble(8));
                d.setCurrentHomeSellerComm(c.getDouble(9));
                d.setCurrentHomeTotalOwed(c.getDouble(10));
                d.setCurrentHomeNetProceeds(c.getDouble(11));
                d.setCurrentHomeTotalDown(c.getDouble(12));
                d.setNewHomeSalePrice(c.getDouble(13));
                d.setNewHomeClosingCosts(c.getDouble(14));
                d.setNewHomeMortgageAmount(c.getDouble(15));
                d.setNewHomeLTV(c.getDouble(16));
                d.setNewHomeBuyerCommPerc(c.getDouble(17));
                d.setNewHomeBuyerComm(c.getDouble(18));
                d.setNewHomeSellerCommPerc(c.getDouble(19));
                d.setNewHomeSellerComm(c.getDouble(20));
                d.setNewHomeEstTaxes(c.getDouble(21));
                d.setNewHomeEstInsurance(c.getDouble(22));
                d.setNewHomeAPR(c.getDouble(23));
                d.setNewHomePI(c.getDouble(24));
                d.setNewHomePITI(c.getDouble(25));
                d.setNewHomeTerm(c.getLong(26));
                d.setPrimaryContact(c.getString(27));
                d.setAlternateContact(c.getString(28));

                list.add(position++,d);
            }
        }finally {
            c.close();
        }

            return list;
    }

    public static void updateCurrentDeal(Deal d, long id, Context context)
    {
        RealToolsDbHelper dbHelper = new RealToolsDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_DEAL_NAME,d.getDealName());
        if (d.isActive())
           values.put(RealtorToolsContract.DealTable.COLUMN_NAME_ACTIVE, "Y");
        else
           values.put(RealtorToolsContract.DealTable.COLUMN_NAME_ACTIVE,"N");
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SALE_PRICE,d.getCurrentHomeSalePrice());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_ADD_CASH,d.getCurrentHomeAdditionalCash());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_CLOSING_COST,d.getCurrentHomeClosingCost());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_BUYER_COMM_PERC,d.getCurrentHomeBuyerCommPerc());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_BUYER_COMM,d.getCurrentHomeBuyerComm());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SELLER_COMM_PERC,d.getCurrentHomeSellerCommPerc());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_SELLER_COMM,d.getCurrentHomeSellerComm());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_TOTAL_OWED,d.getCurrentHomeTotalOwed());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_NET_PROCEEDS,d.getCurrentHomeNetProceeds());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_CURR_HOME_TOTAL_DOWN,d.getCurrentHomeTotalDown());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SALE_PRICE,d.getNewHomeSalePrice());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_CLOSING_COSTS,d.getNewHomeClosingCosts());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_MORT_AMOUNT,d.getNewHomeMortgageAmount());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_LTV,d.getNewHomeLTV());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_BUYER_COMM_PERC,d.getNewHomeBuyerCommPerc());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_BUYER_COMM,d.getNewHomeBuyerComm());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SELLER_COMM_PERC,d.getNewHomeSellerCommPerc());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_SELLER_COMM,d.getNewHomeSellerComm());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_APR,d.getNewHomeAPR());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_PI,d.getNewHomePI());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_PITI,d.getNewHomePITI());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_EST_INS,d.getNewHomeEstInsurance());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_EST_TAXES,d.getNewHomeEstTaxes());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_NEW_HOME_TERM,d.getNewHomeTerm());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_PRIMARY_CONTACT_ID,d.getPrimaryContact());
        values.put(RealtorToolsContract.DealTable.COLUMN_NAME_ALT_CONTACT_ID,d.getAlternateContact());

        String selection = RealtorToolsContract.DealTable._ID + " = ?";
        String[] selectionArgs = {String.valueOf(id)};

        int count = db.update(
                RealtorToolsContract.DealTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);

    }

    public static rtSettings getSettings(Context context)
    {
        RealToolsDbHelper dbHelper = new RealToolsDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        rtSettings settings = new rtSettings();

        String[] columns = {
                RealtorToolsContract.SettingsTable._ID,
                RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,
                RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,
                RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_BLOB,
        };

        String selection = null;
        String[] selectionArgs = null;

        Cursor c = db.query(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                columns,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        try{

            while (c.moveToNext())
            {
                String code = c.getString(1);
                switch (code) {
                    case "AGENT_NAME":
                        settings.setAgentName(c.getString(2));
                        break;
                    case "AGENT_PHONE":
                        settings.setAgentPhone(c.getString(2));
                        break;
                    case "AGENT_EMAIL":
                        settings.setAgentEmail(c.getString(2));
                        break;
                    case "AGENT_TEXT_LINE1":
                        settings.setAgentTextLine1(c.getString(2));
                        break;
                    case "AGENT_TEXT_LINE2":
                        settings.setAgentTextLine2(c.getString(2));
                        break;
                    case "AGENT_TEXT_LINE3":
                        settings.setAgentTextLine3(c.getString(2));
                        break;
                    case "DEFAULT_BUY_COMM_PERC":
                        settings.setDefaultBuyCommPercent(Double.parseDouble(c.getString(2)));
                        break;
                    case "DEFAULT_SELL_COMM_PERC":
                        settings.setDefaultSellCommPercent(Double.parseDouble(c.getString(2)));
                        break;
                    case "DEFAULT_APR":
                        settings.setDefaultAPR(Double.parseDouble(c.getString(2)));
                        break;
                    case "DEFAULT_CLOSING_COST_PERC":
                        settings.setDefaultClosingCostPercent(Double.parseDouble(c.getString(2)));
                        break;
                    case "DEFAULT_CLOSING_COST_AMOUBNT":
                        settings.setDefaultClosingCostAmount(Double.parseDouble(c.getString(2)));
                        break;
                    case "AGENT_PHOTO":
                        settings.setAgentPhoto(c.getString(2));
                        break;

                }
            }
        }finally {
            c.close();
        }

        return settings;

    }



    public static void saveSettings(Context context, rtSettings settings)
    {
        RealToolsDbHelper dbHelper = new RealToolsDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        String selection = RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE + " = ?";
        String[] selectionArgs = {"AGENT_NAME"};


        //Set the agent name
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_NAME");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getAgentName());
        int count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);


        //set the agent phone
        selectionArgs[0] = "AGENT_PHONE";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_PHONE");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getAgentPhone());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        //set the agent photo
        selectionArgs[0] = "AGENT_PHOTO";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_PHOTO");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getAgentPhoto());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        //set the default buy perc
        selectionArgs[0] = "DEFAULT_BUY_COMM_PERC";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"DEFAULT_BUY_COMM_PERC");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getDefaultBuyCommPercent().toString());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);


        //set the default sell perc
        selectionArgs[0] = "DEFAULT_SELL_COMM_PERC";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"DEFAULT_SELL_COMM_PERC");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getDefaultSellCommPercent().toString());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        //set the agemt email address
        selectionArgs[0] = "AGENT_EMAIL";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_EMAIL");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getAgentEmail());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        //set the agent text line1
        selectionArgs[0] = "AGENT_TEXT_LINE1";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_TEXT_LINE1");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getAgentTextLine1());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        //set the agent text line2
        selectionArgs[0] = "AGENT_TEXT_LINE2";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_TEXT_LINE2");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getAgentTextLine2());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        //set the agent text line3
        selectionArgs[0] = "AGENT_TEXT_LINE3";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"AGENT_TEXT_LINE3");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getAgentTextLine3());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        //set the default APR
        selectionArgs[0] = "DEFAULT_APR";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"DEFAULT_APR");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getDefaultSellCommPercent().toString());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        //set the default closing cost percent
        selectionArgs[0] = "DEFAULT_CLOSING_COST_PERC";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"DEFAULT_CLOSING_COST_PERC");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getDefaultClosingCostPercent().toString());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);

        //set the default closing cost amount
        selectionArgs[0] = "DEFAULT_CLOSING_COST_AMOUNT";
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_CODE,"DEFAULT_CLOSING_COST_AMOUNT");
        values.put(RealtorToolsContract.SettingsTable.COLUMN_NAME_SETTING_VALUE,settings.getDefaultClosingCostAmount().toString());
        count = db.update(
                RealtorToolsContract.SettingsTable.TABLE_NAME,
                values,
                selection,
                selectionArgs);

    }


    public static void deleteDeal(Context context, long dealID)
    {
        RealToolsDbHelper dbHelper = new RealToolsDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String selection = RealtorToolsContract.DealTable._ID + " = ?";
        String[] selectionArgs = {String.valueOf(dealID)};
        db.delete(RealtorToolsContract.DealTable.TABLE_NAME,selection,selectionArgs);
    }

    public static Deal applyDealDefaults(Context context, Deal d)
    {

        rtSettings settings = getSettings(context);
        d.setNewHomeSellerCommPerc(settings.getDefaultSellCommPercent());
        d.setNewHomeBuyerCommPerc(settings.getDefaultBuyCommPercent());
        d.setCurrentHomeBuyerCommPerc(settings.getDefaultBuyCommPercent());
        d.setCurrentHomeSellerCommPerc(settings.getDefaultSellCommPercent());
        d.setNewHomeAPR(settings.getDefaultAPR());
        return d;
    }
}
