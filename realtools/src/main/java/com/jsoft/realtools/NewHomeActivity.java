package com.jsoft.realtools;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NewHomeActivity extends Activity implements CommissionPopupFragment.OnCommissionPercentSet{

    Deal deal;
    long curr_deal_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);
        Bundle extras = getIntent().getExtras();

        curr_deal_id = extras.getLong("CURR_DEAL_ID");
        deal=DealToolFileHelper.getCurrentDeal(curr_deal_id,this);

        setData();

        final EditText newHomeSalePriceEdit = (EditText)findViewById(R.id.newHomeSalePrice);
        newHomeSalePriceEdit.setSelectAllOnFocus(true);
        newHomeSalePriceEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                EditText newHomeSalePriceEdit = (EditText)findViewById(R.id.newHomeSalePrice);
                deal.setNewHomeSalePrice(Double.parseDouble(newHomeSalePriceEdit.getText().toString()));
                deal.calcDeal();
                setData();
                return false;
            }
        });

        newHomeSalePriceEdit.setOnFocusChangeListener(new TextView.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v,boolean hasFocus){
                if (!hasFocus){
                    deal.setNewHomeSalePrice(Double.parseDouble(newHomeSalePriceEdit.getText().toString()));
                    deal.calcDeal();
                    setData();
                }
            }
        });
        final Button changeCommButton = (Button) findViewById(R.id.changeCommButton);
            changeCommButton.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                Fragment frag = manager.findFragmentByTag("fragment_commission");
                if (frag != null) {
                    manager.beginTransaction().remove(frag).commit();
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putDouble("buyer_comm",deal.getNewHomeBuyerCommPerc());
                    bundle.putDouble("seller_comm",deal.getNewHomeSellerCommPerc());
                    bundle.putLong("curr_deal_id",deal.getDealID());
                    CommissionPopupFragment commissionFragment = new CommissionPopupFragment();
                    commissionFragment.setArguments(bundle);
                    commissionFragment.show(manager, "fragment_commission");
                }
            }
        });
        final Button summaryButton = (Button) findViewById(R.id.doneButton);

        final EditText newHomeTotalDown = (EditText)findViewById(R.id.totalDownText);
        newHomeTotalDown.setSelectAllOnFocus(true);
        newHomeTotalDown.setOnEditorActionListener(new TextView.OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView v, int actionId,KeyEvent event) {
                EditText newHomeTotalDown = (EditText) findViewById(R.id.totalDownText);
                deal.setCurrentHomeTotalDown(Long.parseLong(newHomeTotalDown.getText().toString()));
                deal.calcDeal();
                setData();
                return false;
            }
        });

        final EditText newHomeAPR = (EditText)findViewById(R.id.newHomeAPR);
        newHomeAPR.setSelectAllOnFocus(true);
        newHomeAPR.setOnEditorActionListener(new TextView.OnEditorActionListener(){
            @Override
            public boolean onEditorAction(TextView v, int actionId,KeyEvent event) {
                EditText newHomeAPR = (EditText) findViewById(R.id.newHomeAPR);
                deal.setNewHomeAPR(Double.parseDouble(newHomeAPR.getText().toString())/100);
                deal.calcDeal();
                setData();
                return false;
            }
        });

        final EditText newHomeLoanTerm = (EditText)findViewById(R.id.newHomeTerm);
        newHomeLoanTerm.setSelectAllOnFocus(true);
        newHomeLoanTerm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                EditText newHomeLoanTerm = (EditText)findViewById(R.id.newHomeTerm);
                deal.setNewHomeTerm(Long.parseLong(newHomeLoanTerm.getText().toString()));
                deal.calcDeal();
                setData();
                return false;
            }
        });

        final EditText newHomeEstTaxes = (EditText)findViewById(R.id.newHomeEstTaxes);
        newHomeEstTaxes.setSelectAllOnFocus(true);
        newHomeEstTaxes.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                EditText newHomeEstTaxes = (EditText)findViewById(R.id.newHomeEstTaxes);
                deal.setNewHomeEstTaxes(Double.parseDouble(newHomeEstTaxes.getText().toString()));
                deal.calcDeal();
                setData();
                return false;
            }
        });

        final EditText newHomeEstInsurance = (EditText)findViewById(R.id.newHomeEstInsurance);
        newHomeEstInsurance.setSelectAllOnFocus(true);
        newHomeEstInsurance.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                EditText newHomeEstInsurance = (EditText)findViewById(R.id.newHomeEstInsurance);
                deal.setNewHomeEstInsurance(Double.parseDouble(newHomeEstInsurance.getText().toString()));
                deal.calcDeal();
                setData();
                return false;
            }
        });


        summaryButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startSummaryActivity();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_realtool, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            FragmentManager manager = getFragmentManager();
            Fragment frag = manager.findFragmentByTag("fragment_settings");
            if (frag != null) {
                manager.beginTransaction().remove(frag).commit();
            } else {
                SettingsFragment settingsFragment = new SettingsFragment();
                settingsFragment.show(manager, "fragment_settings");
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void setData()
    {
        EditText newHomeSalePrice = (EditText) findViewById(R.id.newHomeSalePrice);
        TextView netProceeds = (TextView) findViewById(R.id.totalDownText);
        TextView newMortgage = (TextView) findViewById(R.id.newHomeMortgage);
        TextView mortgageLTV = (TextView) findViewById(R.id.newHomeLTV);
        EditText newHomeEstTaxes = (EditText)findViewById(R.id.newHomeEstTaxes);
        EditText newHomeEstInsurance = (EditText)findViewById(R.id.newHomeEstInsurance);
        EditText newHomeAPR = (EditText)findViewById(R.id.newHomeAPR);
        EditText newHomeTerm = (EditText)findViewById(R.id.newHomeTerm);
        TextView newHomePI = (TextView)findViewById(R.id.newHomePI);
        TextView newHomePITI = (TextView)findViewById(R.id.newHomePITI);

        if (deal.getNewHomeSalePrice()>0) {
            newHomeSalePrice.setText(String.format("%1.2f", deal.getNewHomeSalePrice()));
            netProceeds.setText(String.format("%1.2f", deal.getCurrentHomeNetProceeds()));
            newMortgage.setText(String.format("%1.2f", deal.getNewHomeMortgageAmount()));
            mortgageLTV.setText(String.format("%3.2f", deal.getNewHomeLTV() * 100));
            newHomeAPR.setText(String.format("%3.2f",deal.getNewHomeAPR() * 100));
            newHomeTerm.setText(String.format("%1d",deal.getNewHomeTerm()));
            newHomeEstTaxes.setText(String.format("%1.2f",deal.getNewHomeEstTaxes()));
            newHomeEstInsurance.setText(String.format("%1.2f",deal.getNewHomeEstInsurance()));
            newHomePI.setText(String.format("%1.2f",deal.getNewHomePI()));  
            newHomePITI.setText(String.format("%1.2f",deal.getNewHomePITI()));
        }
        else
        {
            newHomeSalePrice.setText("$0.00");
            netProceeds.setText(String.format("$%1.2f", deal.getCurrentHomeNetProceeds()));
            newMortgage.setText("$0.00");
            mortgageLTV.setText("0.0%");
            newHomeAPR.setText("0.0%");
            newHomeTerm.setText("0");
            newHomeEstTaxes.setText("$0.00");
            newHomeEstInsurance.setText("$0.00");
            newHomePI.setText("$0.00");
            newHomePITI.setText("$0.00");
        }

    }

    private void startSummaryActivity()
    {
        DealToolFileHelper.updateCurrentDeal(deal,curr_deal_id,this);
        finish();
    }

    @Override
    public void onCommissionPercentSet(double sellerCommissionPercent, double buyerCommissionPercent)
    {
        deal.setNewHomeBuyerCommPerc(buyerCommissionPercent/100);
        deal.setNewHomeSellerCommPerc(sellerCommissionPercent / 100);

        deal.calcDeal();
        DealToolFileHelper.updateCurrentDeal(deal,curr_deal_id,this);
        setData();
    }
}
