package com.jsoft.realtools;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class ContactFragment extends DialogFragment implements View.OnClickListener
{
    public ContactFragment() {
        // Required empty public constructor
    }

    Deal deal;
    long curr_deal_id;
    TextView primaryName;
    TextView secondaryName;
    ImageView primaryPhoto;
    ImageView secondaryPhoto;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact, container);
        Bundle extras = getArguments();
        curr_deal_id = extras.getLong("CURR_DEAL_ID");

        deal = DealToolFileHelper.getCurrentDeal(curr_deal_id,getActivity());

        primaryPhoto = (ImageView)view.findViewById(R.id.primaryContactImage);
        secondaryPhoto = (ImageView)view.findViewById(R.id.secondaryContactImage);

        Button okButton = (Button) view.findViewById(R.id.contactOkButton);
        okButton.setOnClickListener(this);

        Button primaryButton = (Button)view.findViewById(R.id.primaryContactButton);
        primaryButton.setOnClickListener(this);

        Button secondaryButton = (Button)view.findViewById(R.id.secondaryContactButton);
        secondaryButton.setOnClickListener(this);

        ImageButton callPrimaryButton = (ImageButton)view.findViewById(R.id.callPrimaryButton);
        callPrimaryButton.setOnClickListener(this);

        ImageButton callSecondaryButton = (ImageButton)view.findViewById(R.id.callSecondaryButton);
        callSecondaryButton.setOnClickListener(this);

        primaryName = (TextView)view.findViewById(R.id.primaryNameText);
        secondaryName = (TextView)view.findViewById(R.id.secondaryNameText);

        if (deal.getPrimaryContact().isEmpty()){
            primaryName.setText("Not Selected");
        }
        else
        {
            Uri primaryContactUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI,deal.getPrimaryContact());
            Uri res = ContactsContract.Contacts.lookupContact(getActivity().getContentResolver(),primaryContactUri);
            Cursor c = getActivity().getContentResolver().query(res,null,null,null,null);

            if (c.moveToFirst()){

                primaryName.setText(c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                InputStream str = ContactsContract.Contacts.openContactPhotoInputStream(getActivity().getContentResolver(),primaryContactUri);
                if (str != null)
                {
                    BufferedInputStream buf = new BufferedInputStream(str);
                    Bitmap bmap = BitmapFactory.decodeStream(buf);
                    primaryPhoto.setImageBitmap(Bitmap.createScaledBitmap(bmap, 240, 240, false));
                }
                else {
                    Drawable image = getResources().getDrawable(R.drawable.ic_blank_person);
                    primaryPhoto.setImageDrawable(image);
                }
            }
        }

        if (deal.getAlternateContact().isEmpty()){
            secondaryName.setText("Not Selected");
        }
        else
        {
            Uri secondaryContactUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI,deal.getAlternateContact());
            Uri res = ContactsContract.Contacts.lookupContact(getActivity().getContentResolver(),secondaryContactUri);
            Cursor c = getActivity().getContentResolver().query(res,null,null,null,null);

            if (c.moveToFirst()){

                secondaryName.setText(c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));
                InputStream str = ContactsContract.Contacts.openContactPhotoInputStream(getActivity().getContentResolver(),secondaryContactUri);
                if (str != null)
                {
                    BufferedInputStream buf = new BufferedInputStream(str);
                    Bitmap bmap = BitmapFactory.decodeStream(buf);
                    secondaryPhoto.setImageBitmap(Bitmap.createScaledBitmap(bmap, 240, 240, false));
                }
                else {
                    Drawable image = getResources().getDrawable(R.drawable.ic_blank_person);
                    secondaryPhoto.setImageDrawable(image);
                }

            }
        }

        return view;

    }


    @Override
    public void onClick(View v) {

        switch(v.getId()){
            case R.id.contactOkButton:
                finishContacts();
                break;
            case R.id.primaryContactButton:
                selectContact(1);
                break;
            case R.id.secondaryContactButton:
                selectContact(2);
                break;
            case R.id.callPrimaryButton:
                callContact(1);
                break;
            case R.id.callSecondaryButton:
                callContact(2);
                break;
        }
    }

    private void finishContacts()
    {

        this.dismiss();
    }

    private void selectContact(int which)
    {
        Intent contactIntent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
        contactIntent.setType(ContactsContract.Contacts.CONTENT_ITEM_TYPE);
        if (contactIntent.resolveActivity(getActivity().getPackageManager())!=null)
        {
            startActivityForResult(contactIntent,which);
        }

    }


    private void callContact(int which) {

        String lookupId = "";

        if (which == 1) {
            lookupId = deal.getPrimaryContact();
        } else {
            lookupId = deal.getAlternateContact();
        }
        if (!lookupId.isEmpty())
        {
            Uri contactUri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI,lookupId);

            Uri res = ContactsContract.Contacts.lookupContact(getActivity().getContentResolver(), contactUri);

            Cursor c = getActivity().getContentResolver().query(res, null, null, null, null);

            if (c.getCount() > 0) {
                Intent intent = new Intent(Intent.ACTION_VIEW, res);

                if (intent.resolveActivity(getActivity().getPackageManager()) != null)
                    startActivityForResult(intent, 3);
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode==1 && resultCode == Activity.RESULT_OK){
            Uri contactUri = data.getData();
            Cursor c = getActivity().getContentResolver().query(contactUri,null,null,null,null);
            if (c.moveToFirst()){
                String lookupId = c.getString(c.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                primaryName.setText(c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));

                InputStream str = ContactsContract.Contacts.openContactPhotoInputStream(getActivity().getContentResolver(),contactUri);
                if (str != null){
                    BufferedInputStream buf = new BufferedInputStream(str);
                    Bitmap bmap = BitmapFactory.decodeStream(buf);
                    primaryPhoto.setImageBitmap(Bitmap.createScaledBitmap(bmap, 240, 240, false));
                }
                else {
                    Drawable image = getResources().getDrawable(R.drawable.ic_blank_person);
                    primaryPhoto.setImageDrawable(image);
                }

                deal.setPrimaryContact(lookupId);
            }
            c.close();
        }
        else if (requestCode==2 && resultCode == Activity.RESULT_OK)
        {
            Uri contactUri = data.getData();
            Cursor c = getActivity().getContentResolver().query(contactUri,null,null,null,null);
            if (c.moveToFirst()) {
                String lookupId = c.getString(c.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                secondaryName.setText(c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)));

                InputStream str = ContactsContract.Contacts.openContactPhotoInputStream(getActivity().getContentResolver(),contactUri);
                if (str!=null) {
                    BufferedInputStream buf = new BufferedInputStream(str);
                    Bitmap bmap = BitmapFactory.decodeStream(buf);
                    secondaryPhoto.setImageBitmap(Bitmap.createScaledBitmap(bmap, 240, 240, false));
                }
                else {
                    Drawable image = getResources().getDrawable(R.drawable.ic_blank_person);
                    secondaryPhoto.setImageDrawable(image);
                }
                deal.setAlternateContact(lookupId);
            }
            c.close();
        }
        DealToolFileHelper.updateCurrentDeal(deal,deal.getDealID(),getActivity());

    }
}
