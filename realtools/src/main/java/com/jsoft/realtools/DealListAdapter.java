package com.jsoft.realtools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by James on 9/2/2015.
 */
public class DealListAdapter extends ArrayAdapter<Deal>
{
    private final Context context;
    private ArrayList<Deal> values;

    public DealListAdapter(Context context, int textViewResourceId,ArrayList values)
    {
        super(context,textViewResourceId,values);
        this.context = context;
        this.values = values;
    }


    public void refresh(ArrayList<Deal> items)
    {
        this.values.clear();
        this.values.addAll(items);
        this.notifyDataSetChanged();
    }
   /* public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(android.R.layout.rowLayout,parent,false);

        return rowView;
    }
    */

}
