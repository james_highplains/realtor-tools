package com.jsoft.realtools;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class CommissionSummaryFragment extends DialogFragment implements View.OnClickListener{

    private TextView currHomeBuyerPerc;
    private TextView currHomeBuyerAmount;
    private TextView currHomeSellerPerc;
    private TextView currHomeSellerAmount;
    private TextView currHomeTotalPerc;
    private TextView currHomeTotalAmount;

    private TextView newHomeBuyerPerc;
    private TextView newHomeBuyerAmount;
    private TextView newHomeSellerPerc;
    private TextView newHomeSellerAmount;
    private TextView newHomeTotalPerc;
    private TextView newHomeTotalAmount;

    private Button okButton;
    Deal curr_deal;
    long curr_deal_id;

    public CommissionSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_commission_summary,container);
        Bundle args = getArguments();
        curr_deal_id = args.getLong("CURR_DEAL_ID");
        curr_deal = DealToolFileHelper.getCurrentDeal(curr_deal_id,getActivity());

        currHomeBuyerPerc = (TextView)view.findViewById(R.id.currHomeBuyerPerc);
        currHomeBuyerAmount = (TextView)view.findViewById(R.id.currHomeBuyerAmount);
        currHomeSellerPerc = (TextView)view.findViewById(R.id.currHomeSellerPerc);
        currHomeSellerAmount = (TextView)view.findViewById(R.id.currHomeSellerAmount);
        currHomeTotalPerc = (TextView)view.findViewById(R.id.currHomeTotalPerc);
        currHomeTotalAmount = (TextView)view.findViewById(R.id.currHomeTotalAmount);

        newHomeBuyerPerc = (TextView)view.findViewById(R.id.newHomeBuyerPerc);
        newHomeBuyerAmount = (TextView)view.findViewById(R.id.newHomeBuyerAmount);
        newHomeSellerPerc = (TextView)view.findViewById(R.id.newHomeSellerPerc);
        newHomeSellerAmount = (TextView)view.findViewById(R.id.newHomeSellerAmount);
        newHomeTotalPerc = (TextView)view.findViewById(R.id.newHomeTotalPerc);
        newHomeTotalAmount = (TextView)view.findViewById(R.id.newHomeTotalAmount);

        getDialog().setTitle("Commission Summary");

        okButton = (Button)view.findViewById(R.id.okButton);
        okButton.setOnClickListener(this);

        setData();
        return view;
    }

    private void setData()
    {
        currHomeBuyerPerc.setText(String.format("%,1.2f%%", curr_deal.getCurrentHomeBuyerCommPerc()*100));
        currHomeBuyerAmount.setText(String.format("$%,1.2f",curr_deal.getCurrentHomeBuyerComm()));
        currHomeSellerPerc.setText(String.format("%,1.2f%%",curr_deal.getCurrentHomeSellerCommPerc()*100));
        currHomeSellerAmount.setText(String.format("$%,1.2f",curr_deal.getCurrentHomeSellerComm()));
        currHomeTotalPerc.setText(String.format("%,1.2f%%",(curr_deal.getCurrentHomeSellerCommPerc()+curr_deal.getCurrentHomeBuyerCommPerc())*100));
        currHomeTotalAmount.setText(String.format("$%,1.2f",curr_deal.getCurrentHomeSellerComm()+curr_deal.getCurrentHomeBuyerComm()));

        newHomeBuyerPerc.setText(String.format("%,1.2f%%",curr_deal.getNewHomeBuyerCommPerc()*100));
        newHomeBuyerAmount.setText(String.format("$%,1.2f",curr_deal.getNewHomeBuyerComm()));
        newHomeSellerPerc.setText(String.format("%,1.2f%%",curr_deal.getNewHomeSellerCommPerc()*100));
        newHomeSellerAmount.setText(String.format("$%,1.2f",curr_deal.getNewHomeSellerComm()));
        newHomeTotalPerc.setText(String.format("%,1.2f%%",(curr_deal.getNewHomeBuyerCommPerc()+curr_deal.getNewHomeSellerCommPerc())*100));
        newHomeTotalAmount.setText(String.format("$%,1.2f",curr_deal.getNewHomeBuyerComm()+curr_deal.getNewHomeSellerComm()));

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onClick(View v)
    {

        this.dismiss();
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        curr_deal = DealToolFileHelper.getCurrentDeal(curr_deal_id,activity.getApplicationContext());
    }
}
