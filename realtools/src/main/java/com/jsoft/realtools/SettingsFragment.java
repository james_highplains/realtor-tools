package com.jsoft.realtools;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;


import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SettingsFragment extends DialogFragment implements View.OnClickListener{

    static final int REQUEST_TAKE_PHOTO = 1;
    String mCurrentPhotoPath = "";

    private Button okButton;

    EditText agentNameEdit;
    EditText agentPhoneEdit;
    EditText agentEmail;
    EditText agentTextLine1;
    EditText agentTextLine2;
    EditText agentTextLine3;
    EditText defaultBuyerCommPerc;
    EditText defaultSellerCommPerc;
    EditText defaultAPR;
    EditText defaultClosingCostPerc;
    EditText defaultClosingCostAmount;
    ImageView agentPhoto;
    rtSettings settings;

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        okButton = (Button)view.findViewById(R.id.settingsOkButton);

        getDialog().setTitle("Enter Default Settings");
        okButton.setOnClickListener(this);

        agentNameEdit = (EditText)view.findViewById(R.id.agentNameEdit);
        agentNameEdit.setSelectAllOnFocus(true);
        agentNameEdit.setShowSoftInputOnFocus(true);
        agentPhoneEdit = (EditText)view.findViewById(R.id.agentPhoneEdit);
        agentEmail = (EditText)view.findViewById(R.id.agentEmailEdit);
        agentTextLine1 = (EditText)view.findViewById(R.id.agentTextLine1);
        agentTextLine2 = (EditText)view.findViewById(R.id.agentTextLine2);
        agentTextLine3 = (EditText)view.findViewById(R.id.agentTextLine3);

        defaultBuyerCommPerc = (EditText)view.findViewById(R.id.defaultBuyerCommEdit);
        defaultSellerCommPerc = (EditText)view.findViewById(R.id.defaultSellerCommEdit);
        defaultAPR = (EditText)view.findViewById(R.id.defaultAPR);
        defaultClosingCostPerc = (EditText)view.findViewById(R.id.defaultClosingCostPerc);
        defaultClosingCostAmount = (EditText)view.findViewById(R.id.defaultClosingCostAmount);
        agentPhoto = (ImageView)view.findViewById(R.id.agentPhotoImageView);

        settings = DealToolFileHelper.getSettings(getActivity());
        agentNameEdit.setText(settings.getAgentName());
        agentPhoneEdit.setText(settings.getAgentPhone());
        agentEmail.setText(settings.getAgentEmail());
        agentTextLine1.setText(settings.getAgentTextLine1());
        agentTextLine2.setText(settings.getAgentTextLine2());
        agentTextLine3.setText(settings.getAgentTextLine3());

        if (!settings.getAgentPhoto().isEmpty()) {
            File photoFile = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),settings.getAgentPhoto());
            Uri photoURI = FileProvider.getUriForFile(getActivity(), "com.jsoft.realtools.fileprovider", photoFile);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getActivity().getContentResolver(),photoURI);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mCurrentPhotoPath = photoFile.getAbsolutePath();
            rotateImage(setReducedImageSize());
        }
        else
        {
            //TODO: Put in code to display blank image
            String uri = "drawable/ic_blank_person.png";

            Drawable image = getResources().getDrawable(R.drawable.ic_blank_person);
            agentPhoto.setImageDrawable(image);
        }

        defaultBuyerCommPerc.setText(String.format("%1.2f",settings.getDefaultBuyCommPercent()*100));
        defaultSellerCommPerc.setText(String.format("%1.2f",settings.getDefaultSellCommPercent()*100));
        defaultAPR.setText(String.format("%1.2f",settings.getDefaultAPR()*100));
        defaultClosingCostPerc.setText(String.format("%1.2f",settings.getDefaultClosingCostPercent()*100));
        defaultClosingCostAmount.setText(String.format("%1.2f",settings.getDefaultClosingCostAmount()));


        agentPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                if (takePictureIntent.resolveActivity(getActivity().getPackageManager())!=null)
                {
                    File photoFile = null;
                    try
                    {
                        photoFile = createImageFile();
                    }catch (IOException ex)
                    {
                        ex.printStackTrace();
                    }
                    if (photoFile !=null){
                        Uri photoURI = FileProvider.getUriForFile(getActivity(),"com.jsoft.realtools.fileprovider",
                                photoFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoURI);
                        settings.setAgentPhoto(photoURI.getLastPathSegment());
                        startActivityForResult(takePictureIntent,REQUEST_TAKE_PHOTO);
                    }
                }

            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK)
        {
            File photoFile = new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),settings.getAgentPhoto());
            rotateImage(setReducedImageSize());

        }
    }

    private Bitmap setReducedImageSize(){
        int targetImageViewWidth = agentPhoto.getWidth();
        int targetImageViewHeight = agentPhoto.getHeight();

        if (targetImageViewWidth == 0 || targetImageViewHeight==0){
            Resources res = getResources();
            targetImageViewWidth = ((Float)res.getDimension(R.dimen.agent_photo)).intValue();
            targetImageViewHeight = ((Float)res.getDimension(R.dimen.agent_photo)).intValue();
        }
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(mCurrentPhotoPath,bmOptions);
        int cameraImageWidth = bmOptions.outWidth;
        int cameraImageHeight = bmOptions.outHeight;

        int scaleFactor = Math.min(cameraImageWidth/targetImageViewWidth,cameraImageHeight/targetImageViewHeight);
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inJustDecodeBounds=false;

        return BitmapFactory.decodeFile(mCurrentPhotoPath,bmOptions);
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

    }


    @Override
    public void onClick(View v)
    {
        settings.setAgentName(String.format("%s",agentNameEdit.getText()));
        settings.setAgentPhone(String.format("%s",agentPhoneEdit.getText()));
        settings.setAgentEmail(String.format("%s",agentEmail.getText()));
        settings.setAgentTextLine1((String.format("%s",agentTextLine1.getText())));
        settings.setAgentTextLine2((String.format("%s",agentTextLine2.getText())));
        settings.setAgentTextLine3((String.format("%s",agentTextLine3.getText())));
        settings.setDefaultBuyCommPercent(Double.parseDouble(defaultBuyerCommPerc.getText().toString())/100);
        settings.setDefaultSellCommPercent(Double.parseDouble(defaultSellerCommPerc.getText().toString())/100);
        settings.setDefaultAPR(Double.parseDouble(defaultAPR.getText().toString())/100);
        settings.setDefaultClosingCostPercent(Double.parseDouble(defaultClosingCostPerc.getText().toString())/100);
        settings.setDefaultClosingCostAmount(Double.parseDouble(defaultClosingCostAmount.getText().toString()));

        DealToolFileHelper.saveSettings(getActivity(),settings);

        this.dismiss();
    }

    private void rotateImage(Bitmap bitmap){
        ExifInterface exifInterface = null;
        try{
            exifInterface = new ExifInterface(mCurrentPhotoPath);
        }catch (IOException e){
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,ExifInterface.ORIENTATION_UNDEFINED);

        Matrix matrix = new Matrix();
        switch(orientation){
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            default:
        }
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);
        agentPhoto.setImageBitmap(rotatedBitmap);
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "RT_AGENT_PHOTO_"+timeStamp+"_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir);

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

}
