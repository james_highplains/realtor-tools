package com.jsoft.realtools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.net.Uri;
import android.os.IBinder;
import android.os.PersistableBundle;

import android.os.Bundle;
import android.os.RemoteException;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.android.vending.billing.IInAppBillingService;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class RealtoolActivity extends Activity {

    private ArrayList dealList;
    private ListView listView;
    private RealToolsDbHelper connection;
    private DealListAdapter adapter;
    IInAppBillingService mService;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    ServiceConnection mServiceConn = new ServiceConnection(){
        @Override
        public void onServiceDisconnected(ComponentName name){
            mService=null;
        }

        @Override
        public void onServiceConnected(ComponentName name,IBinder service){
            mService=IInAppBillingService.Stub.asInterface(service);
        }
    };

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realtool);

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        bindService(serviceIntent,mServiceConn, Context.BIND_AUTO_CREATE);

        connection = new RealToolsDbHelper(this);

        dealList = DealToolFileHelper.getAllCurrentDeals(this);
        adapter = new DealListAdapter(this, android.R.layout.simple_list_item_1, dealList);

        listView = (ListView) findViewById(R.id.dealListView);
        listView.setAdapter(adapter);
        listView.setLongClickable(true);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                deleteDeal(position);
                return true;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startSummaryActivity(position);
            }
        });


        final Button addNewDealButton = (Button) findViewById(R.id.createDealButton);

        addNewDealButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                startNewDealActivity();
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void deleteDeal(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        Deal deal = (Deal) listView.getAdapter().getItem(position);
        final long dealID = deal.getDealID();
        builder.setMessage("Delete " + deal.getDealName() + " deal?");
        builder.setTitle("Confirm Delete");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DealToolFileHelper.deleteDeal(getApplicationContext(), dealID);
                //dealList = DealToolFileHelper.getAllCurrentDeals(getApplicationContext());
                ((DealListAdapter) listView.getAdapter()).refresh(DealToolFileHelper.getAllCurrentDeals(getApplicationContext()));
                //adapter.notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //initializeDealList();
        ((DealListAdapter) listView.getAdapter()).refresh(DealToolFileHelper.getAllCurrentDeals(getApplicationContext()));
        //dealList = DealToolFileHelper.getAllCurrentDeals(getApplicationContext());
        //adapter.notifyDataSetChanged();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
        ((DealListAdapter) listView.getAdapter()).refresh(DealToolFileHelper.getAllCurrentDeals(getApplicationContext()));
        //dealList = DealToolFileHelper.getAllCurrentDeals(getApplicationContext());
        //adapter.notifyDataSetChanged();
    }

    private void initializeDealList() {
        //dealList = DealToolFileHelper.getAllCurrentDeals(this);

        DealListAdapter adapter = new DealListAdapter(this, android.R.layout.simple_list_item_1, DealToolFileHelper.getAllCurrentDeals(this));

        listView.setAdapter(adapter);
    }

    private void startSummaryActivity(int position) {
        Intent intent = new Intent(this, DealSummaryActivity.class);
        Deal deal = (Deal) listView.getAdapter().getItem(position);
        intent.putExtra("START_MODE", true);
        intent.putExtra("CURR_DEAL_ID", deal.getDealID());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_realtool, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            FragmentManager manager = getFragmentManager();
            Fragment frag = manager.findFragmentByTag("fragment_settings");
            if (frag != null) {
                manager.beginTransaction().remove(frag).commit();
            } else {
                SettingsFragment settingsFragment = new SettingsFragment();
                settingsFragment.show(manager, "fragment_settings");
                return true;
            }
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        if (requestCode == 1001){
            int responseCode = data.getIntExtra("RESPONSE_CODE",0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");
            if (resultCode== RESULT_OK){
                try{
                    JSONObject jo = new JSONObject(purchaseData);
                    String sku = jo.getString("productId");

                }
                catch(JSONException e){
                    e.printStackTrace();
                }

            }
        }
    }

    public void startNewDealActivity() {
        //Check how many deals
        //Check if unlimited deals subscription purchased and valid
        //If not: offer to purchase
        //If so: start activity

        boolean unlimitedDealsPurchased = false;

        if (dealList.size()<1)
        {
            //If less than 1 then go ahead and do the deal
            Intent intent = new Intent(this, DealSummaryActivity.class);
            //Put in code to indicate new deal
            intent.putExtra("START_MODE", false);
            intent.putExtra("CURR_DEAL_ID", 0);
            startActivity(intent);

        }
        else
        {
            //If 1 or more check on subscription
            ArrayList<String> skuList = new ArrayList<String>();
            skuList.add("unlimited");
            try {
                Bundle ownedItems = mService.getPurchases(3,getPackageName(),"subs",null);
                int response = ownedItems.getInt("RESPONSE_CODE");
                if (response==0){
                    ArrayList<String> ownedSkus = ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                    ArrayList<String> purchasedDataList = ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                    ArrayList<String> signatureList = ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                    String continuationToken = ownedItems.getString("INAPP_CONTINUATION_TOKEN");

                    for (int i=0;i<purchasedDataList.size();i++){
                        String purchasedData = purchasedDataList.get(i);
                        String signature = signatureList.get(i);
                        String sku = ownedSkus.get(i);

                        if (sku.equalsIgnoreCase("unlimited")) {

                            unlimitedDealsPurchased = true;
                        }
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            if (!unlimitedDealsPurchased)
            {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("A monthly subscription is required to maintain more than one deal.  Would you like to purchase a subscription now?");
                builder.setTitle("Subscription Required");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        //start purchase flow
                        try {
                            Bundle buyIntentBundle = mService.getBuyIntent(3,getPackageName(),"unlimited","subs",null);
                            if (buyIntentBundle.getInt("RESPONSE_CODE")==0){
                                PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
                                startIntentSenderForResult(pendingIntent.getIntentSender(),1001,new Intent(), Integer.valueOf(0),Integer.valueOf(0),Integer.valueOf(0));
                            }

                        } catch (RemoteException e) {
                            e.printStackTrace();
                        } catch (IntentSender.SendIntentException se){
                            se.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        //cancel the dialog and don't start the deal summary activity
                    }
                });
                builder.show();
            }
        }
        if (unlimitedDealsPurchased) {
            Intent intent = new Intent(this, DealSummaryActivity.class);
            intent.putExtra("START_MODE", false);
            intent.putExtra("CURR_DEAL_ID", 0);
            startActivity(intent);
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Realtool Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.jsoft.realtools/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Realtool Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.jsoft.realtools/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        if (mService!=null)
            unbindService(mServiceConn);
    }
}
